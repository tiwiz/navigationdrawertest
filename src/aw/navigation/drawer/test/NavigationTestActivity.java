package aw.navigation.drawer.test;

import android.app.Activity;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v4.app.ActionBarDrawerToggle;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import com.larswerkman.colorpicker.ColorPicker;

public class NavigationTestActivity extends Activity implements View.OnClickListener
{

    private DrawerLayout          mDrawerLayout;
    private ActionBarDrawerToggle mDrawerToggle;
    private CharSequence          mDrawerTitle;
    private CharSequence          mTitle;
    private ColorPicker           picker;
    private Button                btnCambiaColore;

    /**
     * Called when the activity is first created.
     */
    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);

        /**
         * Dichiariamo il bottone e il ColorPicker
         */
        btnCambiaColore = (Button)findViewById(R.id.btnImposta);
        btnCambiaColore.setOnClickListener(this);
        picker = (ColorPicker)findViewById(R.id.picker);

        mDrawerLayout = (DrawerLayout)findViewById(R.id.drawer_layout);
        /**
         * Modifichiamo l'ombra del nostro layout, in modo da creare un effetto tridimensionale
         */
        mDrawerLayout.setDrawerShadow(R.drawable.drawer_shadow, GravityCompat.START);

        /**
         * Rendiamo cliccabile il tasto home
         */
        getActionBar().setDisplayHomeAsUpEnabled(true);
        getActionBar().setHomeButtonEnabled(true);

        mDrawerToggle = new ActionBarDrawerToggle(
                this,                  /* la nostra activity */
                mDrawerLayout,         /* il nostro DrawerLayout */
                R.drawable.ic_drawer,  /* l'immagine per modificare il toggle di fianco alla home */
                R.string.drawer_open,  /* Descrizione dell'apertura del drawable - Accessibilità */
                R.string.drawer_close  /* Descrizione della chiusura del drawable - Accessibilità */
        ) {
            public void onDrawerClosed(View view) {
                getActionBar().setTitle(getString(R.string.drawer_close));
                /**
                 * Se volessimo cambiare a runtime i nostri bottoni (ad esempio la barra della ricerca),
                 * dovremmo richiamare
                 * ******* invalidateOptionsMenu();
                 * che va a sua volta a chiamare
                 * ******* boolean onPrepareOptionsMenu(Menu menu);
                 * che noi dovremo ovverridare per eseguire i nostri cambiamenti
                 */
            }

            public void onDrawerOpened(View drawerView) {
                getActionBar().setTitle(getString(R.string.drawer_open));
            }
        };
        mDrawerLayout.setDrawerListener(mDrawerToggle);
    }

    /**
     * Quando usiamo l'icona personalizzata, come quella animata, dobbiamo richiamare questi due metodi
     * in modo da segnalare ad Android che abbiamo modificato il toggle: in questo modo, potremo ascoltare gli eventi
     * apertura e chiusura del Navigation Drawer e agire di conseguenza.
     */

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        mDrawerToggle.syncState();
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        mDrawerToggle.onConfigurationChanged(newConfig);
    }

    /**
     * Come sappiamo, questo metodo serve per gestire i bottoni dell'ActionBar, home inclusa
     */
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        // Con questo controllo, capiamo se è stato il tasto Home ad essere premuto
        if (mDrawerToggle.onOptionsItemSelected(item)) {
            return true;
        }

        //Qui gestiamo i nostri altri bottoni

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onClick(View view)
    {
        switch(view.getId()){
        case R.id.btnImposta:
            int newColor = picker.getColor();
            picker.setOldCenterColor(newColor);
            break;
        }
    }
}
